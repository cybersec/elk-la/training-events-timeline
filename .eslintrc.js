module.exports = {
  root: true,
  extends: ['@elastic/eslint-config-kibana', 'plugin:@elastic/eui/recommended'],
  rules: {
    '@kbn/eslint/require-license-header': 'off',
    'no-console': 'off',
    'prettier/prettier': 'off',
    '@typescript-eslint/no-unnecessary-type-assertion': 'off',
    '@typescript-eslint/array-type': 'off'
  },
};
