# Plugin installation

## Prerequisite

Running ELK in version 7.12.1 from this repository. The recommendation for system resources is 4GB RAM provided to the Docker.

https://gitlab.fi.muni.cz/cybersec/elk-portal-commands-events 

## Installations

Run installation in Kibana container

`docker exec -it kibana bin/kibana-plugin install https://gitlab.fi.muni.cz/api/v4/projects/20153/packages/generic/kibana-plugin/7.12.1/trainingEventsTimeline-7.12.1.zip`

Restart Kibana container

`docker restart kibana`

Now there should be in Kibana on the last line in menu visible **Training Events Timeline**

## Indexing data

If you have successfully imported data in Elasticsearch, you need to build an index pattern to work with them in Kibana. Go to Menu > Management > Stack management, select Index Patterns on the left side, and then click Create index pattern. In the field, you have to specify your data index pattern. Use the template: 

`kypo.*_evt.pool=*.sandbox=*.definition=*.instance=*.run=*` 

and replace the asterisk with your specific value for data set. For example when you want to include all data with run definition id 5 and 8, use the string:

`kypo.*_evt.pool=*.sandbox=*.definition=5.instance=*.run=*, kypo.*_evt.pool=*.sandbox=*.definition=8.instance=*.run=*`


You can check the listed indexes below to be sure that the correct data are selected. In the next step should be chosen field with the timestamp and that is complete.

## Example usage

Upload testing data via ELK Command Events tool. The datasets are `locust-3302-may-2020` and `knowledge-base-october-2021`.

For successfully imported data, the index pattern needs to be created. Here is OK to create a universal index pattern, which covers both of them

`kypo.*_evt.pool=*.sandbox=*.definition=*.instance=*.run=*` 

and choose as timestamp field `syslog.@timestamp`. 

Go to the plugin `Training Events Timeline` in the left menu and select created index for data in `locust-3302-may-2020`, select the date range to May 2020 and for `knowledge-base-october-2021` for October 2021.

Select the Training run IDs in the select box, and the visualization should appear.

## Known issues

#### Exceeding open shards in Elasticsearch
There is a possibility that when large datasets are imported, Elasticsearch may report an error about exceeding maximum opened shards. The problem might solve more Swap memory (the tested minimum 2 GB) given to Docker in recourses. If there is still a problem with importing data to Elasticsearch, try smaller datasets or have imported in Elasticsearch only one at a time.
