import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { DataPublicPluginStart, IndexPattern } from 'src/plugins/data/public';

interface IUniqueIds {
  runId: number;
  count: number;
}

interface IBucket {
  doc_count: number;
  key: number;
}

function buildRequest(data: DataPublicPluginStart, index: IndexPattern, size: number) {
  const query = data.query.getEsQuery(index);
  return {
    params: {
      index: index.title,
      body: {
        aggs: {
          uniqueRunIds: {
            terms: {
              field: 'training_run_id',
            },
          },
        },
        query: {
          ...query,
          bool: {
            ...query.bool,
            filter: [query.bool.filter[1]],
          },
        },
      },
      size,
    },
  };
}

/**
 * Api request to get all unique IDs in Elasticsearch index pattern
 * @param props$ the Observable contains Data plugin and  index pattern 
 * as [DataPublicPluginStart, IndexPattern | null | undefined]
 * @param timeRefresh$ the Observable detecting that time selection changed
 * @returns the 'hits' atribute from Elasticsearch response
 */
export function getUniqueRunIds(
  props$: Observable<[DataPublicPluginStart, IndexPattern | null | undefined]>,
  timeRefresh: Observable<unknown>
): Observable<IUniqueIds[]> {
  return combineLatest([props$, timeRefresh]).pipe(
    map(([[data, index], time]) =>
      data && index
        ? { data, request: buildRequest(data, index, 9999) }
        : { data: null, request: null }
    ),
    switchMap(({ data, request }) => {
      return data && request
        ? data.search
            .search(request)
            .pipe(
              map(
                (response) =>
                  response?.rawResponse?.aggregations?.uniqueRunIds?.buckets.map(
                    (bucket: IBucket) => ({ runId: bucket.key, count: bucket.doc_count })
                  ) || []
              )
            )
        : of([]);
    }),
    tap((response) => console.log('[AGGS] ANSWER', response))
  );
}
