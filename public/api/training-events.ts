import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { DataPublicPluginStart, IndexPattern } from 'src/plugins/data/public';
import { IKYPOEvent } from '../interfaces';
import { parseEvents } from '../utils';

function buildRequest(
  data: DataPublicPluginStart,
  index: IndexPattern,
  size: number,
  runIds: number[]
) {
  const query = data.query.getEsQuery(index);
  return {
    params: {
      index: index.title,
      body: {
        query: {
          ...query,
          bool: {
            ...query.bool,
            filter: [query.bool.filter[1], { terms: { training_run_id: runIds } }],
          },
        },
      },
      size,
    },
  };
}

/**
 * Api request to get all training event data from Elasticsearch
 * @param props$ the Observable contains Data plugin, index pattern and 
 * training run IDs as [DataPublicPluginStart, IndexPattern | null | undefined, { label: string }[]]
 * @param timeRefresh$ the Observable detecting that time selection changed
 * @returns the 'hits' atribute from Elasticsearch response
 */
export function getTrainingEvents(
  props$: Observable<[DataPublicPluginStart, IndexPattern | null | undefined, { label: string }[]]>,
  timeRefresh$: Observable<unknown>
): Observable<IKYPOEvent[]> {
  return combineLatest([props$, timeRefresh$]).pipe(
    map(([[data, index, runIds], time]) =>
      data && index
        ? {
            data,
            request: buildRequest(
              data,
              index,
              9999,
              runIds.map((run) => +run.label)
            ),
          }
        : { data: null, request: null }
    ),
    switchMap(({ data, request }) => {
      return data && request
        ? data.search
            .search(request)
            .pipe(map((response) => parseEvents(response.rawResponse.hits.hits) || []))
        : of([]);
    }),
    tap((response) => console.log('[EVENTS] ANSWER', response))
  );
}
