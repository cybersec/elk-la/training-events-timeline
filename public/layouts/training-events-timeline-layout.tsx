import React, { useEffect, useState } from 'react';
import { i18n } from '@kbn/i18n';
import { I18nProvider } from '@kbn/i18n/react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  EuiFlexGroup,
  EuiPage,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody,
  EuiPageHeader,
  EuiTitle,
  EuiFlexItem,
  EuiFormLabel,
  EuiComboBox,
} from '@elastic/eui';
import { useObservable, useSubscription } from 'observable-hooks';
import { getUniqueRunIds, getTrainingEvents } from '../api';

import { DataPublicPluginStart, IndexPattern } from '../../../../src/plugins/data/public';
import { EventsTimelineChart } from '../components/events-timeline-chart';
import { CoreStart } from '../../../../src/core/public';
import { NavigationPublicPluginStart } from '../../../../src/plugins/navigation/public';

import { PLUGIN_ID } from '../../common';
import { IKYPOEvent } from '../interfaces';

interface TrainingEventsTimelineAppDeps {
  basename: string;
  notifications: CoreStart['notifications'];
  http: CoreStart['http'];
  navigation: NavigationPublicPluginStart;
  data: DataPublicPluginStart;
}

/**
 * Main layout of the Training events timeline with setup field for 
 * index pattern, training run IDs and timeline diagram itself.
 * */
export const TrainingEventsTimelineLayout = ({ basename, navigation, data }: TrainingEventsTimelineAppDeps) => {
  const { IndexPatternSelect } = data.ui;
  const [eventsData, setEventsData] = useState<IKYPOEvent[]>([]);
  const [indexPattern, setIndexPattern] = useState<IndexPattern | null>();

  const [runIdsOptions, setRunIdsOptions] = useState<{label: string}[]>([]);
  const [selectedRunIds, setSelectedRunIds] = useState<{label: string}[]>([]);
  const uniqueRunIds$ = useObservable((inputs$) => getUniqueRunIds(inputs$, data.query.timefilter.timefilter.getTimeUpdate$()), [data, indexPattern])
  const trainingEvents$ = useObservable((inputs$) => getTrainingEvents(inputs$, data.query.timefilter.timefilter.getTimeUpdate$()), [data, indexPattern, selectedRunIds])

  useSubscription(uniqueRunIds$, (response) => setRunIdsOptions(response.map((opt) => ({ label: opt.runId.toString() }))));
  useSubscription(trainingEvents$, (response) => setEventsData(response));

  useEffect(() => {
    const setDefaultIndexPattern = async () => {
      const defaultIndexPattern = await data.indexPatterns.getDefault();
      setIndexPattern(defaultIndexPattern);
    };
    setDefaultIndexPattern();
  }, [data]);

  return (
    <Router basename={basename}>
      <I18nProvider>
        <>
          <navigation.ui.TopNavMenu
            appName={PLUGIN_ID}
            showSearchBar={true}
            useDefaultBehaviors={true}
          />
          <EuiPage restrictWidth="1000px">
            <EuiPageBody>
              <EuiPageHeader>
                <EuiTitle size="l">
                  <h1>Training Progress</h1>
                </EuiTitle>
              </EuiPageHeader>
              <EuiPageContent>
                <EuiPageContentBody>
                  <EuiFlexGroup >
                    <EuiFlexItem>
                        <EuiFormLabel>Index Pattern</EuiFormLabel>
                        <IndexPatternSelect
                          placeholder={i18n.translate(
                            'searchSessionExample.selectIndexPatternPlaceholder',
                            {
                              defaultMessage: 'Select index pattern',
                            }
                          )}
                          indexPatternId={indexPattern?.id || ''}
                          onChange={async (newIndexPatternId: any) => {
                            const newIndexPattern = await data.indexPatterns.get(
                              newIndexPatternId
                            );
                            setIndexPattern(newIndexPattern);
                          }}
                          isClearable={false}
                        />
                    </EuiFlexItem>
                    <EuiFlexItem>
                        <EuiFormLabel>Run IDs selection</EuiFormLabel>
                        <EuiComboBox
                          placeholder="Select run ID"
                          options={runIdsOptions}
                          selectedOptions={selectedRunIds}
                          onChange={(selOpts) => setSelectedRunIds(selOpts)}
                          isClearable={true}
                        />
                    </EuiFlexItem>
                  </EuiFlexGroup>
                  <EventsTimelineChart events={eventsData} />
                </EuiPageContentBody>
              </EuiPageContent>
            </EuiPageBody>
          </EuiPage>
        </>
      </I18nProvider>
    </Router>
  );
};
