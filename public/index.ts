import './index.scss';

import { TrainingEventsTimelinePlugin } from './plugin';

// This exports static code and TypeScript types,
// as well as, Kibana Platform `plugin()` initializer.
export function plugin() {
  return new TrainingEventsTimelinePlugin();
}
export { TrainingEventsTimelinePluginSetup, TrainingEventsTimelinePluginStart } from './types';
