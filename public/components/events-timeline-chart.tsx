import React, { useState, useEffect, useRef } from 'react';

import * as d3 from 'd3';
import { EuiCheckbox, EuiFlexGrid, EuiFlexItem } from '@elastic/eui';
import { IKYPOEvent, IKYPOEventType } from '../interfaces';
import { KypoEventsMap } from '../utils';

interface IProps {
  events: IKYPOEvent[];
}

const PASTEL_COLORS: string[] = [
  '#414833',
  '#656D4A',
  '#A4AC86',
  '#C2C5AA',
  '#B6AD90',
  '#A68A64',
  '#936639',
  '#7F4F24'
];

/**
 * This component takes the events on input and render the timeline diagram.
 * @param events the input list of IKYPOEvent
 */
export const EventsTimelineChart = ({ events }: IProps) => {
  const canvasContainer = useRef<HTMLDivElement>(null);
  const width = 840;
  const height = 500;
  const [selectedEvents, setSelectedEvents] = useState<{ [eventId: string]: boolean }>({});

  useEffect(() => {
    const tempSelectedEvents: { [eventId: string]: boolean } = {};
    Object.keys(KypoEventsMap).forEach((eventId) => (tempSelectedEvents[eventId] = true));
    setSelectedEvents(tempSelectedEvents);
  }, []);

  useEffect(() => {
    if (canvasContainer.current === undefined) return;

    const lineData = getUniqueRunIds(events);
    const levels = events.filter((e) => e.type === 'LevelStart');
    const levelColors = colorizeLevels(levels);
    const maxScore = findMaxScore(events, lineData);
    const singleTimeEventsData = events.filter(
      (e) => e.type === IKYPOEventType.SingleTimeEvent && selectedEvents[e.typeKey]
    );

    const svg = d3
      .select(canvasContainer.current)
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .style('position', 'relative');

    const tooltip = createTooltipRef(canvasContainer.current)
    const tooltipChart = d3
      .select(canvasContainer.current)
      .append('div')
      .style('opacity', 0)
      .attr('class', 'tooptip__chart')
      .style('background-color', 'white')
      .style('border', '2px solid #717070')
      .style('border-radius', '5px')
      .style('position', 'absolute');
    const tooltipChartSvg = tooltipChart.append('svg').attr('width', 180).attr('height', 160);

    const lines = svg.selectAll('line').data(lineData);
    const longEvents = svg.selectAll('line').data(levels);
    const totalScore = svg.selectAll('text').data([null, ...maxScore]);
    const singleTimeEvents = svg.selectAll('rect').data(singleTimeEventsData);

    const [xScale, yScale] = createScales(width, height, lineData, events);

    const yAxis = d3.axisLeft(yScale);
    const xAxis = d3.axisTop(xScale).ticks(7).tickFormat(d3.timeFormat('%d. %m. %H:%M') as any);
    svg.append('g').call(yAxis).attr('transform', 'translate(25, 0)');
    svg.append('g').call(xAxis).attr('transform', 'translate(0, 35)');

    createRunIdLines(lines, width, yScale);

    longEvents
      .enter()
      .append('line')
      .attr('x1', (d) => xScale(d.timestamp))
      .attr('x2', (d) => (d.timestampEnd ? xScale(d.timestampEnd) - 5 : xScale(d.timestamp)))
      .attr('y1', (d) => yScale(d.trainingRunId.toString()) as number)
      .attr('y2', (d) => yScale(d.trainingRunId.toString()) as number)
      .attr('stroke', (d, i) => levelColors[d.levelId].formatHex())
      .attr('stroke-width', 9)
      .on('mouseover', function (event, d) {
        d3.select(this).attr('stroke', levelColors[d.levelId].darker(1).formatHex());
        tooltip.style('opacity', 1);
      })
      .on('mousemove', function (event, d) {
        tooltip
          .html(
            d.levelTitle +
              '<br />' +
              formatDate(d.timestamp) +
              ' - ' +
              (d?.timestampEnd ? formatDate(d.timestampEnd) : '') +
              '<br />' +
              'Actual score in level: ' +
              d.actualScore
          )
          .style('left', d3.pointer(event)[0] + 70 + 'px')
          .style('top', d3.pointer(event)[1] + 'px')
          .style('color', 'white');
      })
      .on('mouseout', function (event, d) {
        d3.select(this).attr('stroke', levelColors[d.levelId].formatHex());
        tooltip.style('opacity', 0);
      });

    totalScore
      .enter()
      .append('text')
      .text((d) => d?.totalScore ?? 'Total score')
      .attr('x', (d) => (d ? width - 40 : width - 60))
      .attr('y', (d) => (d ? (yScale(d.runId.toString()) as number) + 4 : 35))
      .style('font-size', '0.7rem')
      .on('mouseover', function (event, d) {
        tooltipChart.style('opacity', 1);
      })
      .on('mousemove', function (event, d) {
        tooltipChart
          .style('opacity', 1)
          .style('left', d3.pointer(event)[0] + 50 + 'px')
          .style('top', d3.pointer(event)[1] + 'px');

        tooltipChartSvg.selectAll('rect').remove();
        tooltipChartSvg.selectAll('text').remove();
        tooltipChartSvg.selectAll('g').remove();

        const tooltipData = events.filter((e) => e.type === 'LevelEnd' && e.trainingRunId === (d?.runId || 0));

        const xScaleTip = d3
          .scalePoint()
          .domain(tooltipData.map((td) => td.levelId.toString()))
          .range([20, 180 - 30]);
        const yScaleTip = d3
          .scaleLinear()
          .domain(d3.extent(tooltipData, (td) => td.totalScore) as [number, number])
          .range([10, 120]);

        const xAxisTip = d3.axisBottom(xScaleTip);
        tooltipChartSvg.append('g').call(xAxisTip).attr('transform', 'translate(5, 140)');

        tooltipChartSvg
          .selectAll('rect')
          .data(tooltipData)
          .enter()
          .append('rect')
          .attr('x', (td) => xScaleTip(td.levelId.toString()) as number)
          .attr('y', (td) => 135 - yScaleTip(td.totalScore))
          .attr('width', 10)
          .attr('height', (td) => yScaleTip(td.totalScore))
          .attr('fill', 'green');

        tooltipChartSvg
          .selectAll('text')
          .data(tooltipData)
          .enter()
          .append('text')
          .text((td) => td.totalScore ? td.totalScore.toString() : 'xx')
          .attr('x', (td) => (xScaleTip(td.levelId.toString()) as number) - 2)
          .attr('y', (td) => 135 - yScaleTip(td.totalScore) - 5)
          .attr('font-size', 10)
          .attr('fill', 'red')
      })
      .on('mouseout', function (event, d) {
        tooltipChart.style('opacity', 0);
      });

    singleTimeEvents
      .enter()
      .append('rect')
      .attr('x', (d) => xScale(d.timestamp))
      .attr('y', (d) => yScale(d.trainingRunId.toString()) as number)
      .attr('width', 20)
      .attr('height', 20)
      .attr('rx', 5)
      .attr('transform', (d) => `rotate(-45, ${xScale(d.timestamp)}, ${yScale(d.trainingRunId.toString()) as number}) translate(-14.14, -14.14)`)
      .attr('fill', (d) => d.color)
      .attr('style', 'filter: drop-shadow(0px 0px 2px rgb(0 0 0 / 0.25))')
      .on('mouseover', function (event, d) {
        d3.select(this).transition()
          .duration(200)
          .attr('y', yScale(d.trainingRunId.toString()) as number - 5)
          .attr('transform', `rotate(-45, ${xScale(d.timestamp)}, ${yScale(d.trainingRunId.toString()) as number - 5}) translate(-14.14, -14.14)`)
        tooltip.style('opacity', 1);
      })
      .on('mousemove', function (event, d) {
        tooltip
          .html(
            formatDate(d.timestamp) +
              ' ' +
              d.title +
              '<br />' +
              'Actual score in level: ' +
              d.actualScore + '<br />' + `${['WrongAnswerSubmitted', 'CorrectAnswerSubmitted', 'CorrectFlagSubmitted', 'WrongFlagSubmitted'].includes(d.typeKey) ? 'Submited answer: ' + d.flagContent || '' : ''}`,              
          )
          .style('left', d3.pointer(event)[0] + 70 + 'px')
          .style('top', d3.pointer(event)[1] + 'px')
          .style('color', 'white');
      })
      .on('mouseout', function (event, d) {
        d3.select(this).transition()
          .duration(200)
          .attr('y', yScale(d.trainingRunId.toString()) as number)
          .attr('transform', `rotate(-45, ${xScale(d.timestamp)}, ${yScale(d.trainingRunId.toString()) as number}) translate(-14.14, -14.14)`)
        tooltip.style('opacity', 0);
      });

    return () => {
      svg.remove();
    };
  }, [events, selectedEvents]);

  const checkEvent = (eventId: string, checked: boolean) => {
    setSelectedEvents({ ...selectedEvents, [eventId]: checked });
  };

  return (
    <div>
      <div style={{ position: 'relative' }} ref={canvasContainer} />
      <EuiFlexGrid columns={4}>
        {Object.keys(KypoEventsMap).filter((eventId) => (KypoEventsMap[eventId].type !== IKYPOEventType.LevelStart) && (KypoEventsMap[eventId].type !== IKYPOEventType.LevelEnd)).map((eventId) => (
          <EuiFlexItem>
            <EuiCheckbox
              id={eventId}
              checked={selectedEvents[eventId]}
              onChange={(e) => checkEvent(eventId, e.target.checked)}
              label={KypoEventsMap[eventId].title}
            />
          </EuiFlexItem>
        ))}
      </EuiFlexGrid>
    </div>
  );
};


/**
 * Return unique training run IDs from given data
 * @param rawData list of IKYPOEvent
 * @returns list of training run IDs
 */
function getUniqueRunIds(rawData: IKYPOEvent[]): string[] {
  const uniqueIds = new Set<number>();
  rawData.forEach((item) => uniqueIds.add(item.trainingRunId));
  return [...uniqueIds].map((item) => item.toString());
}


/**
 * Format time to hh:mm:ss
 * @param date input as Date
 * @returns formated time as string
 */
function formatDate(date: Date) {
  return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}


/**
 * Give color from pallete defined in `PASTEL_COLORS` to each level
 * @param levels list of IKYPOEvent
 * @returns `{ [levelId: number]: d3.RGBColor }`
 */
function colorizeLevels(levels: IKYPOEvent[]): { [levelId: number]: d3.RGBColor } {
  const outputColors: { [levelId: number]: d3.RGBColor } = {};
  const uniqueLevels: Set<number> = new Set<number>();
  levels.forEach((level) => uniqueLevels.add(level.levelId));
  [...uniqueLevels].sort((a, b) => +a - +b).forEach(
    (levelId, index) => (outputColors[levelId] = d3.rgb(PASTEL_COLORS[index]) || d3.rgb('black'))
  );
  return outputColors;
}


/**
 * Find in training events highest score for given training run IDs
 * @param events list of IKYPOEvent
 * @param runIds list of training run IDs
 * @returns 
 */
function findMaxScore(
  events: IKYPOEvent[],
  runIds: string[]
): Array<{ totalScore: number; runId: number }> {
  const maxScore: { [runId: string]: number } = {};
  runIds.forEach((runId) => (maxScore[runId] = 0));
  events.forEach((event) => {
    if (maxScore[event.trainingRunId] < (event.totalScore || 0))
      maxScore[event.trainingRunId] = event.totalScore;
  });
  return Object.keys(maxScore).map((runId) => ({ runId: +runId, totalScore: maxScore[runId] }));
}


/**
 * Create X and Y scale for training events timeline chart
 * @param width canvas width
 * @param height canvas height
 * @param xDomain datetime domain
 * @param yDomain training run IDs domain
 * @returns `[ScalePoint, ScaleTime]`
 */
function createScales(width: number, height: number, xDomain: any[], yDomain: any[]): [d3.ScaleTime<number, number, never>, d3.ScalePoint<string>] {
  const yScale = d3
      .scalePoint()
      .domain(xDomain)
      .range([60, height - 60]);
  const xScale = d3
    .scaleTime()
    .domain(d3.extent(yDomain, (d) => d.timestamp) as [Date, Date])
    .range([40, width - 70]);
  return [xScale, yScale]
}


/**
 * Create line for each training run ID in diagram
 * @param lines existing selection for the diagram lines
 * @param width canvas width
 * @param yScale Y scale as `ScalePoint`
 * @returns selection with appended lines
 */
function createRunIdLines(lines: d3.Selection<d3.BaseType, string, SVGSVGElement, unknown>, width: number, yScale: d3.ScalePoint<string>) {
  return lines.enter()
    .append('line')
    .attr('x1', 40)
    .attr('x2', width - 70)
    .attr('y1', (d) => yScale(d) as number)
    .attr('y2', (d) => yScale(d) as number)
    .attr('stroke', 'black');
}


/**
 * Create tooltip reference for training events and levels
 * @param canvasContainer main SVG canvas
 * @returns canvas with appended invisible tooltip
 */
function createTooltipRef(canvasContainer: any): d3.Selection<any, unknown, null, undefined> {
  return d3.select(canvasContainer)
    .append('div')
    .style('opacity', 0)
    .attr('class', 'tooptip')
    .style('background-color', '#717070')
    .style('border-radius', '20px')
    .style('padding', '20px')
    .style('line-height', '1.5')
    .style('position', 'absolute');
}