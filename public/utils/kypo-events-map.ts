import { IKYPOEventType } from '../interfaces';

const COLORS = {
    correct: '#2A9D8F',
    warning: '#E9C46A',
    error: '#E76F51',
    neutral: '#DDBEA9',
    levels: '#E76F51'
}


/**
 * This map is used to parse the raw events the Elasticsearch. The benefit is the data unification
 * to the local interperation.
 */
export const KypoEventsMap: { [id: string]: { title: string, color: string, type: IKYPOEventType } } = {
    ['AssessmentAnswers']: {
        title: 'Assessment Answers',
        color: COLORS.neutral,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['TrainingRunResumed']: {
        title: 'Training Run Resumed',
        color: COLORS.levels,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['TrainingRunStarted']: {
        title: 'Training Run Started',
        color: COLORS.levels,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['SolutionDisplayed']: {
        title: 'Solution Displayed',
        color: COLORS.warning,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['WrongFlagSubmitted']: {
        title: 'Wrong Flag Submitted',
        color: COLORS.error,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['WrongAnswerSubmitted']: {
        title: 'Wrong Answer Submitted',
        color: COLORS.error,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['HintTaken']: {
        title: 'Hint Taken',
        color: COLORS.warning,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['LevelStarted']: {
        title: 'Level Started',
        color: COLORS.levels,
        type: IKYPOEventType.LevelStart
    },
    ['PhaseStarted']: {
        title: 'Phase Started',
        color: COLORS.levels,
        type: IKYPOEventType.LevelStart
    },
    ['LevelCompleted']: {
        title: 'Level Completed',
        color: COLORS.levels,
        type: IKYPOEventType.LevelEnd
    },
    ['PhaseCompleted']: {
        title: 'Phase Completed',
        color: COLORS.levels,
        type: IKYPOEventType.LevelEnd
    },
    ['CorrectFlagSubmitted']: {
        title: 'Correct Flag Submitted',
        color: COLORS.correct,
        type: IKYPOEventType.SingleTimeEvent
    },
    ['CorrectAnswerSubmitted']: {
        title: 'Correct Answer Submitted',
        color: COLORS.correct,
        type: IKYPOEventType.SingleTimeEvent
    }
}