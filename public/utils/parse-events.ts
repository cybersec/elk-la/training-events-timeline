import { IKYPOEvent, IKYPOEventType } from '../interfaces';
import { KypoEventsMap } from './kypo-events-map';
import { groupEventByRunId } from './group-event-by-run-id';


/**
 * Takes the raw response from the ELasticsearch and transform
 * each event to the IKYPOEvent interface.
 * @param rawData the response from the Elasticsearch
 * @returns list of IKYPOEvent
 */
export function parseEvents(rawData: any[]): IKYPOEvent[] {

    if(!(rawData && rawData.length > 0)) return [];

    let outputData: IKYPOEvent[] = mappingEvents(rawData);
    outputData = addTimestampEnd(outputData)
    return outputData;
}

/**
 * Mapping and transform each event to `IKYPOEvent`
 * @param rawData the response from the Elasticsearch
 * @returns list of `IKYPOEvent`
 */
function mappingEvents(rawData: any[]): IKYPOEvent[] {
    return rawData.map((line) => ({
        timestamp: new Date(line._source.syslog['@timestamp']),
        trainingInstanceId: line._source.training_instance_id,
        trainingDefinitionId: line._source.training_definition_id,
        trainingRunId: line._source.training_run_id,
        sandboxId: line._source.sandbox_id,
        typeKey: extractKey(line._source.type),
        title: extractTitle(line._source.type),
        color: extractColor(line._source.type),
        type: extractType(line._source.type),
        timestampEnd: null,
        levelTitle: line._source.level_title || line._source.phase_title || null,
        levelId: +line._source.level || +line._source.phase_id,
        totalScore: +line._source.total_score,
        actualScore: +line._source.actual_score_in_level,
        flagContent: line._source.flag_content || line._source.answer_content || null
    }))
}

/**
 * Parsing end timestamp atribute to `IKYPOEventType.LevelStart` event 
 * @param data list of `IKYPOEvent`
 * @returns list of `IKYPOEvent`
 */
function addTimestampEnd(data: IKYPOEvent[]): IKYPOEvent[] {
    const grouped = groupEventByRunId(data);
    const outputData: IKYPOEvent[] = [];
    Object.keys(grouped).forEach(runId => {
        const runIdEvents = grouped[+runId];
        runIdEvents.forEach((event, index) => {
            if(event.type === IKYPOEventType.LevelStart) {
                outputData.push({...event, timestampEnd: runIdEvents.find((sEvent, sIndex) => ((sIndex > index) && (sEvent.type === IKYPOEventType.LevelEnd)))?.timestamp || null})
            } else {
                outputData.push(event)
            }
        })
    })
    return outputData;
}

/**
 * Extract type of the event
 * @param rawKey atribute from raw Elasticsearch response
 * @returns extracted event type
 */
function extractKey(rawKey: string): string {
    const path = rawKey.split('.');
    return path[path.length - 1];
}


/**
 * Assign title to the event type
 * @param rawKey atribute from raw Elasticsearch response
 * @returns assigned event title
 */
function extractTitle(rawKey: string): string {
    return KypoEventsMap[extractKey(rawKey)]?.title || '';
}


/**
 * Assign color to the event type
 * @param rawKey atribute from raw Elasticsearch response
 * @returns assigned event color
 */
function extractColor(rawKey: string): string {
    return KypoEventsMap[extractKey(rawKey)]?.color || '';
}


/**
 * Extract type of the event from extracted key event
 * @param rawKey atribute from raw Elasticsearch response
 * @returns extracted event type
 */
function extractType(rawKey: string): IKYPOEventType {
    return KypoEventsMap[extractKey(rawKey)]?.type || IKYPOEventType.SingleTimeEvent;
}