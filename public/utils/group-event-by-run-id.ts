import { IKYPOEvent } from '../interfaces';

/**
 * This function gets the list of IKYPOEvent and group them by training run ID atribute
 * @param data list of IKYPOEvent
 * @returns object { [runId: number]: IKYPOEvent[] }
 */
export function groupEventByRunId(data: IKYPOEvent[]): { [runId: number]: IKYPOEvent[] } {
  const groupedEvents: { [runId: number]: IKYPOEvent[] } = {};
  data.forEach((line) =>
    line.trainingRunId in groupedEvents
      ? groupedEvents[line.trainingRunId].push(line)
      : (groupedEvents[line.trainingRunId] = [line])
  );
  Object.keys(groupedEvents).forEach((runId) => groupedEvents[+runId].sort(sortByDateAcs));
  return groupedEvents;
}

/**
 * Sort the `IKYPOEvent` by timestamp
 * @param a event
 * @param b event
 * @returns standard comparison result
 */
export function sortByDateAcs(a: IKYPOEvent, b: IKYPOEvent): number {
  const aDate = new Date(a.timestamp);
  const bDate = new Date(b.timestamp);
  return aDate.valueOf() - bDate.valueOf();
}