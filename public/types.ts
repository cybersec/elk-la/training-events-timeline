import { NavigationPublicPluginStart } from '../../../src/plugins/navigation/public';
import { DataPublicPluginStart } from '../../../src/plugins/data/public';

export interface TrainingEventsTimelinePluginSetup {}
export interface TrainingEventsTimelinePluginStart {}

export interface AppPluginSetupDependencies {}

export interface AppPluginStartDependencies {
  navigation: NavigationPublicPluginStart;
  data: DataPublicPluginStart;
}
