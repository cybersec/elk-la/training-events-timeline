export enum IKYPOEventType {
    'LevelStart' = 'LevelStart',
    'LevelEnd' = 'LevelEnd',
    'SingleTimeEvent' = 'SingleTimeEvent'
}

export interface IKYPOEvent {
    type: IKYPOEventType,

    timestamp: Date,
    timestampEnd: Date | null,

    trainingInstanceId: number,
    trainingDefinitionId: number,
    trainingRunId: number,
    sandboxId: number,
    typeKey: string,
    title: string,
    color: string,
    levelId: number,
    totalScore: number,
    actualScore: number,
    flagContent: string | null,

    levelTitle: string | null
}