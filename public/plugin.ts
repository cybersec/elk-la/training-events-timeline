import { AppMountParameters, CoreSetup, CoreStart, Plugin } from '../../../src/core/public';
import {
  TrainingEventsTimelinePluginSetup,
  TrainingEventsTimelinePluginStart,
  AppPluginSetupDependencies,
  AppPluginStartDependencies,
} from './types';
import { PLUGIN_NAME } from '../common';

export class TrainingEventsTimelinePlugin
  implements Plugin<TrainingEventsTimelinePluginSetup, TrainingEventsTimelinePluginStart, AppPluginSetupDependencies, AppPluginStartDependencies> {
  public setup(core: CoreSetup<AppPluginStartDependencies>, {}: AppPluginSetupDependencies): TrainingEventsTimelinePluginSetup {
    // Register an application into the side navigation menu
    core.application.register({
      id: 'trainingEventsTimeline',
      title: PLUGIN_NAME,
      async mount(params: AppMountParameters) {
        // Load application bundle
        const { renderApp } = await import('./application');
        // Get start services as specified in kibana.json
        const [coreStart, depsStart] = await core.getStartServices();
        // Render the application
        return renderApp(coreStart, depsStart as AppPluginStartDependencies, params);
      },
    });

    // Return methods that should be available to other plugins
    return {};
  }

  public start(core: CoreStart): TrainingEventsTimelinePluginStart {
    return {};
  }

  public stop() {}
}
